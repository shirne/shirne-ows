{php}if(empty($bannerflag)){ $bannerflag = 'channel_'.$channel['id']; }{/php}
{extendtag:advs var="banners" flag="$bannerflag"/}
<div class="swiper-container subbanner">
    <div class="swiper-wrapper">
        {volist name="banners" id="item" key="k"}
            <div class="swiper-slide" style="background-image:url({$item.image});height:100%;">
                <img class="mainbg" src="{$item.image}" alt="{$image.title}">
                {if !empty($item['elements'])}
                    {volist name="item['elements']" id="ele"}
                        {if $ele['type']=='image'}
                        <img class="ani" src="{$ele.image}" style="{$ele.style}" swiper-animate-effect="{$ele.effect}" swiper-animate-duration="{$ele.duration}s" swiper-animate-delay="{$ele.delay}s" />
                        {else/}
                            <p class="ani" style="{$ele.style}" swiper-animate-effect="{$ele.effect}" swiper-animate-duration="{$ele.duration}s" swiper-animate-delay="{$ele.delay}s">{$ele.text}</p>
                        {/if}
                    {/volist}
                {/if}
            </div>
        {/volist}
    </div>
    
    <div class="swiper-pagination"></div>
</div>
<script type="text/javascript">
    var mySwiper = new Swiper ('.subbanner', {
        watchSlidesProgress: true,
	slidesPerView: 'auto',
	centeredSlides: true,
	loop: true,
	loopedSlides: 5,
	autoplay: false,
	navigation: {
		nextEl: '.subbanner .swiper-button-next',
		prevEl: '.subbanner .swiper-button-prev',
	},
	pagination: {
		el: '.subbanner .swiper-pagination',
		//clickable :true,
	},
    on:{
      init: function(){
        if(this.slides.length < 1)return;
        swiperAnimateCache(this);
        swiperAnimate(this);
      }, 
      slideChangeTransitionEnd: function(){ 
        if(this.slides.length < 1)return;
        swiperAnimate(this);
      } 
    }
  })
    </script>