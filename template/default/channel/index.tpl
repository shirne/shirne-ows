{extend name="public:base"/}

{block name="body"}
    <div class="main">
        {include file="channel:_banner" /}
        
        <div class="breadcrumb-box wow slideInUp" data-wow-duration="0.8s">
			<div class="container">
				<nav aria-label="breadcrumb" >
					<ol class="breadcrumb">
						<li class="breadcrumb-item" ><i class="ion-md-pin" name="breadcrumb"></i> 首页</li>
                        {volist name="categoryTree" id="c"}
						<li class="breadcrumb-item" >{$c['title']}</li>
                        {/volist}
					</ol>
				  </nav>
			</div>
		</div>

    <div class="container">
      <div class="row">
          {include file="channel:_side" /}
          <div class="col wow slideInRight"  data-wow-delay="0.5s" data-wow-duration="0.8s">
              <div class="channel-index-body" >
                {volist name="categories[$channel['id']]" id="cate"}
                <div class="card mb-3">
                    <div class="card-header">
                    <a href="{:listurl($cate['name'])}" target="_blank">{$cate['title']}</a>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2 m-0">
                            {article:list category="$cate['id']" recursive="true" var="artlist" /}
                            {volist name="artlist" id="art"}
                            <a href="{:viewurl($art)}" class="btn btn-link text-left col-12 col-lg-6" target="_blank">{$art['title']}</a>
                            {/volist}
                        </div>
                    </div>
                </div>
                {/volist}
              </div>
          </div>
      </div>
  </div>
{/block}