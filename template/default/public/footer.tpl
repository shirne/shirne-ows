

<div class="footer">
    
    <div class="container">
        
        <div class="row wow slideInUp" data-wow-duration="0.8s">
            <div class="col">
                <img src="/static/images/logo-gray.png" alt="" srcset="">
            </div>
            <div class="col">
                <div class="service-telephone float-right">
                    {$config['site-telephone']}
                    <div class="icons">
                        <div class="icon-item icon-wechat">
                            <div class="qrcode"><img src="{$config['site-qrcode']}" alt="" srcset=""></div>
                        </div>
                        <a class="icon-item icon-weibo" href="{$config['site-weibo']}" rel="nofollow"></a>
                        <a class="icon-item icon-qq" href="{$config['site-servqq']}" rel="nofollow"></a>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-4 wow slideInUp" data-wow-duration="0.8s"/>
        <div class="copyright-row wow slideInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">
            <div class="text-center">
                <div class="botton-nav">
                    {volist name="navigator" id="nav"}
                        <a class="bnav-link" href="{$nav['url']}" target="{$nav['target']}">{$nav['title']}</a>
                    {/volist}
                    <a class="bnav-link" href="{:url('index/index/sitemap')}" target="{$nav['target']}">网站地图</a>
                </div>
                版权所有&copy;{:date('Y')} {$config['site-name']} ALL RIGHTS RESERVED<br />
                电话：{$config['site-telephone']}E-mail：{$config['site-email']}<br />
                地址：{$config['site-address']}<br />
                <a href="https://beian.miit.gov.cn/" target="_blank">{$config['site-icp']}</a>{$config['site-tongji']|raw}&nbsp;
                {if !empty($config['gongan-icp'])}
                    {php}$icpcode=preg_replace('/[^\d]+/','',$config['gongan-icp']){/php}
                    <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode={$icpcode}" target="_blank"> <img src="__STATIC__/images/beianicon.png" style="vertical-align: middle;" /> {$config['gongan-icp']}</a>
                {/if}
            </div>
        </div>
    </div>
</div>