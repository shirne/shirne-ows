<div class="feedbackrow">
    <div class="container">
        <div class="row wow slideInUp" data-wow-duration="0.8s">
            <div class="col">
                <h4 class="text-center">算一算我家装修需要多少钱？</h4>
                <div class="clear-fix">
                    <form class="form-inline" name="feedback">
                        <div class="form-group mb-2">
                            <label for="realname" class="sr-only">称呼姓名</label>
                            <input type="text" class="form-control" name="realname" id="realname" placeholder="称呼姓名" >
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="mobile" class="sr-only">联系电话</label>
                            <input type="text" class="form-control" name="mobile" id="mobile" placeholder="联系电话" >
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="description" class="sr-only">楼盘/小区名称/房屋面积/其他需求</label>
                            <input type="text" class="form-control" name="description" id="description" placeholder="楼盘/小区名称/房屋面积/其他需求" >
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">提交项目需求</button>
                    </form>
                </div>
                <div class="help-block">*请认真填写需求信息，我们会在24小时内与您取得联系</div>
            </div>
        </div>
    </div>
    <script>
        jQuery(function($){
            $('[name=feedback]').submit(function(e){
                e.preventDefault();
                var form = $(this);
                var btn =form.find('[type=submit]');
                var realnameField=form.find('[name=realname]');
                var mobileField=form.find('[name=mobile]');
                var descriptionField=form.find('[name=description]');
                
                var data = form.serialize();
                if(!realnameField.val()){
                    dialog.warning('请填写姓名');
                    realnameField.focus();
                    return false;
                }
                if(!mobileField.val()){
                    dialog.warning('请填写电话');
                    mobileField.focus();
                    return false;
                }
                if(!mobileField.val().match(/^1\d{10}$/) && !mobileField.val().match(/^(\d+-)*\d{7,8}$/)){
                    dialog.warning('请填写正确的联系电话');
                    mobileField.focus();
                    return false;
                }
                if(!descriptionField.val()){
                    dialog.warning('请填写需求');
                    descriptionField.focus();
                    return false;
                }
                btn.prop('disabled',true);
                $.ajax({
                    url:"{:url('api/common/do_feedback')}",
                    type:'POST',
                    dataType:'json',
                    data:data,
                    success:function(json){
                        if(json.code == 1){
                            dialog.success('您的需求已提交');
                            form[0].reset();
                        }else{
                            dialog.warning(json.msg);
                        }
                        btn.prop('disabled',false);
                    }
                })

                return false;
            });
        })
    </script>
</div>