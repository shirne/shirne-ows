{extend name="public:base" /}

{block name="body"}
	<div class="main">
		{include file="channel:_banner" /}

		<div class="breadcrumb-box wow slideInUp" data-wow-duration="0.8s">
			<div class="container">
				<span class="float-right">{$article['title']}</span>
				<nav aria-label="breadcrumb" >
					<ol class="breadcrumb">
						<li class="breadcrumb-item" ><i class="ion-md-pin" name="breadcrumb"></i> 首页</li>
						{volist name="categoryTree" id="c"}
                            <li class="breadcrumb-item" >{$c['title']}</li>
                        {/volist}
					</ol>
				  </nav>
			</div>
		</div>
		<div class="container">
			<div class="row">
				{include file="channel:_side" /}
				<div class="col wow slideInRight"  data-wow-delay="0.5s" data-wow-duration="0.8s">
					
					<div class="view-body shadow" >
					{if !empty($article['cover'])}
						<img src="{$article['cover']}" class="img-fluid" alt="{$article['title']}">
					{/if}
						<div class="article-body">
							<div class="page-content">
								{$article.content|raw}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
{/block}
