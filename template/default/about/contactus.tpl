{extend name="public:base" /}

{block name="body"}
	<div class="main">
		{include file="channel:_banner" /}

		<div class="breadcrumb-box wow slideInUp" data-wow-duration="0.8s">
			<div class="container">
				<span class="float-right">{$article['title']}</span>
				<nav aria-label="breadcrumb" >
					<ol class="breadcrumb">
						<li class="breadcrumb-item" ><i class="ion-md-pin" name="breadcrumb"></i> 首页</li>
						{volist name="categoryTree" id="c"}
                            <li class="breadcrumb-item" >{$c['title']}</li>
                        {/volist}
					</ol>
				  </nav>
			</div>
		</div>
		<div class="container">
			<div class="row">
				{include file="channel:_side" /}
				<div class="col wow slideInRight"  data-wow-delay="0.5s" data-wow-duration="0.8s">
					
					<div class="view-body shadow" >
					{if !empty($article['cover'])}
						<img src="{$article['cover']}" class="img-fluid" alt="{$article['title']}">
					{/if}
						<div class="article-body">
							<div class="page-content">
								{$article.content|raw}
							</div>
							<div id="tencentmap" style="width: 100%;height: 350px;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
{/block}

{block name="script"}
	<script>
	function tmapReady() {
		var location = "{$config['site-location']}".split(',')
		var center = new qq.maps.LatLng(parseFloat(location[1]),parseFloat(location[0]));
		var map = new qq.maps.Map(document.getElementById('tencentmap'),{
			center: center,
			zoom: 13
		});
		var infoWin = new qq.maps.InfoWindow({
			map: map
		});
		infoWin.open();
		infoWin.setContent("<h5>{$config['site-name']}</h5><div>地址：{$config['site-address']}<br />电话：{$config['site-telephone']}</div>");
		infoWin.setPosition(map.getCenter());
		
	}
	</script>
	<script type="text/javascript" charset="utf-8" src="https://map.qq.com/api/js?v=2.exp&key={$config['mapkey_tencent']}&callback=tmapReady"></script>
{/block}
