{extend name="public:base"/}
{block name="body"}
<div class="sitemap-body">
    <div class="container ">
        <h2 class="p-3"><i class="ion-md-map"></i>站点地图</h2>
        {volist name="categories[0]" id="channel"}
        {if getSetting('sitemap_html') == '2'}
        <div class="card mb-3">
            <div class="card-header">
            <a href="{:indexurl($channel['name'])}" target="_blank">{$channel['title']}</a>
            </div>
            <div class="card-body">
                {if $channel['channel_mode'] == 1}
                <div class="row">
                    {volist name="categories[$channel['id']]" id="cate"}
                    <a href="{:listurl($cate['name'],$channel['name'])}" class="btn btn-link col-3" target="_blank">{$cate['title']}</a>
                    {/volist}
                </div>
                {else/}
                {volist name="categories[$channel['id']]" id="cate"}
                <a href="{:listurl($cate['name'],$channel['name'])}" class="btn btn-link btn-block text-left border mb-2">{$cate['title']}</a>
                
                <div class="row mb-2 m-0">
                    {article:list category="$cate['id']" recursive="true" var="artlist" /}
                    {volist name="artlist" id="art"}
                    <a href="{:viewurl($art)}" class="btn btn-link text-left col-12 col-lg-6" target="_blank">{$art['title']}</a>
                    {/volist}
                </div>
                {/volist}
                {/if}
            </div>
        </div>
        {else/}
        <div class="card mb-3">
            <div class="card-header">
            <a href="{:indexurl($channel['name'])}" target="_blank">{$channel['title']}</a>
            </div>
            <div class="card-body">
                <div class="row">
                    {volist name="categories[$channel['id']]" id="cate"}
                    <a href="{:listurl($cate['name'],$channel['name'])}" class="btn btn-link col-3" target="_blank">{$cate['title']}</a>
                    {volist name="categories[$cate['id']]" id="cate2"}
                    <a href="{:listurl($cate2['name'],$channel['name'])}" class="btn btn-link col-3" target="_blank">{$cate2['title']}</a>
                    {volist name="categories[$cate2['id']]" id="cate3"}
                    <a href="{:listurl($cate3['name'],$channel['name'])}" class="btn btn-link col-3" target="_blank">{$cate3['title']}</a>
                    {/volist}
                    {/volist}
                    {/volist}
                </div>
            </div>
        </div>
        {/if}
        {/volist}
    </div>
</div>
{/block}
{block name="script"}

{/block}
