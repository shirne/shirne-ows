{extend name="public:base"/}
{block name="body"}
<div class="page-index">
    {extendtag:advs var="banners" flag="banner"/}
    <div id="swiperbanner" class="swiper-container" style="height: 600px;background:#f0f0f0">
        <div class="swiper-wrapper">
            {volist name="banners" id="item" key="k"}
                <div class="swiper-slide" style="background-image:url({$item.image});height:1760px;">
                    <img class="mainbg" src="{$item.image}" alt="{$image.title}">
                    {if !empty($item['elements'])}
                        {volist name="item['elements']" id="ele"}
                            {if $ele['type']=='image'}
                            <img class="ani" src="{$ele.image}" style="{$ele.style}" swiper-animate-effect="{$ele.effect}" swiper-animate-duration="{$ele.duration}s" swiper-animate-delay="{$ele.delay}s" />
                            {else/}
                                <p class="ani" style="{$ele.style}" swiper-animate-effect="{$ele.effect}" swiper-animate-duration="{$ele.duration}s" swiper-animate-delay="{$ele.delay}s">{$ele.text}</p>
                            {/if}
                        {/volist}
                    {/if}
                </div>
            {/volist}
        </div>
        
        <div class="swiper-pagination"></div>
    </div>
    
    <div class="page-block">
        
        <div class="subblock profile-block wow slideInUp" data-wow-duration="0.8s">
            <div class="container">
                <div class="row">
                    <div class="block-title">
                        <span class="title-en">services</span>
                        <span class="title-cn">企业简介</span>
                        <span class="description">走进{$config['site-company']}</span>
                    </div>
                </div>
                <div class="row no-gutters bg-light">
                    {article:find var="about" name="profile" /}
                    <div class="col-6 p-3">
                        {$about.content|raw}
                    </div>
                    <div class="col-6">
                        <img src="{$about.icon|default='/static/images/about.png'}" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
        
        <div class="subblock news-block block-hasbg wow slideInUp" data-wow-duration="0.8s">
            <div class="container">
                <div class="row ">
                    <div class="block-title">
                        <span class="title-en">news and infomation</span>
                        <span class="title-cn">新闻资讯</span>
                        <span class="description">走进{$config['site-company']}</span>
                    </div>
                </div>
                <div class="list-wrapper">
                {article:list var="news" category="infomation" recursive="true" limit="5"/}
                {volist name="news" id="p"}
                <div class="card bg-transparent border-0">
                    <div class="row no-gutters">
                        {if !empty($p['cover'])}
                        <div class="col img-col">
                            <img src="{$p.cover|media='small'}" class="mr-3" alt="{$p.title}">
                        </div>
                        {/if}
                        <div class="col">
                            <div class="card-body">
                                <span class="float-right badge badge-secondary">{$p.create_time|showdate='Y-m-d'}</span>
                                <h5 class="mt-0">{$p.title}</h5>
                                <p class="desc">{$p.description}</p>
                            </div>
                        </div>
                    </div>
                </div>
                {/volist}
                </div>
            </div>
        </div>
    </div>
</div>
{/block}
{block name="script"}
    <script type="text/javascript">
    var mySwiper = new Swiper ('#swiperbanner', {
    on:{
      init: function(){
        if(this.slides.length < 1)return;
        swiperAnimateCache(this);
        swiperAnimate(this);
      }, 
      slideChangeTransitionEnd: function(){ 
        if(this.slides.length < 1)return;
        swiperAnimate(this);
      } 
    }
  })
    </script>
{/block}
