{extend name="public:base"/}
{block name="body"}

<div class="main">
    <div class="container">
        <div class="article-body">
            <div>
                <form>
                    <div class="form-row align-items-center">
                      <div class="col-auto">
                        <label class="sr-only" for="inlineFormInputGroup">关键字</label>
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <i class="input-group-text ion-md-search"></i>
                          </div>
                          <input type="text" class="form-control" name="keyword" value="{$keyword}" id="inlineFormInputGroup" placeholder="关键字">
                        </div>
                      </div>
                      <div class="col-auto">
                        <button type="submit" class="btn btn-primary mb-2">搜索</button>
                      </div>
                      <div class="col-auto">
                        热门关键字：{extendtag:keywords var="keywords" limit="3"}
                        {volist name="keywords" id="word"}
                        <a href="{:url('index/index/search')}?keyword={$word.title}" title="搜索 {$word.title}">{$word.title}</a>
                        {/volist}
                      </div>
                    </div>
                  </form>
            </div>
            <div>“<b>{$keyword}</b>” 共搜索到 {$totalCount} 条内容</div>
            <div class="article-content">
                <div class="search-list">
                    {volist name="lists" id="item"}
                    <a href="{:viewurl($item)}" title="{$item.title}" target="_blank" class="media mb-3">
                        {if !empty($item['cover'])}
                        <img src="{$item.cover}?w=100&h=100&m=out" class="mr-3" alt="{$item.title}">
                        {/if}
                        <div class="media-body">
                          <h5 class="mt-0">{$item.title}</h5>
                          <p>{$item.description}</p>
                        </div>
                    </a>
                    {/volist}
                </div>
                {$page|raw}
            </div>
        </div>
    </div>
</div>
{/block}
{block name="script"}
{/block}