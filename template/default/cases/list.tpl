{extend name="public:base"/}

{block name="body"}
    <div class="main">
        {include file="channel:_banner" /}

        <div class="breadcrumb-box wow slideInUp" data-wow-duration="0.8s">
			<div class="container">
				<nav aria-label="breadcrumb" >
					<ol class="breadcrumb">
						<li class="breadcrumb-item" ><i class="ion-md-pin" name="breadcrumb"></i> 首页</li>
                        {volist name="categoryTree" id="c"}
						<li class="breadcrumb-item" >{$c['title']}</li>
                        {/volist}
					</ol>
				  </nav>
			</div>
		</div>

        <div class="container">
			<div class="row">
				{include file="channel:_side" /}

				<div class="col wow slideInRight"  data-wow-delay="0.5s" data-wow-duration="0.8s">

                    <div class="view-body shadow" >
                        <div class="case-list">
                            <div class="row">
                            {php}$empty='<div class="col-12 empty-box"><p class="empty">暂时没有内容</p></div>';{/php}
                            {volist name="lists" id="art" empty="$empty"}
                                <div class="col-6 mb-4">
                                <div class="card text-white">
                                    {if !empty($art['cover'])}
                                    <img src="{$art['cover']}" class="card-img" alt="{$art.title}">
                                    {else/}
                                    <svg class="bd-placeholder-img bd-placeholder-img-lg card-img" width="100%" height="270" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Card image" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#6c757d"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">{$art.title|cutstr=1}</text></svg>
                                {/if}
                                    <a href="{:url('index/channel/view',['channel_name'=>$channel['name'],'cate_name'=>$art['category_name'],'article_name'=>$art['name']])}" class="card-img-overlay">
                                      <h5 class="card-title">{$art.title}</h5>
                                      <p class="card-text">{$art.description}</p>
                                      <p class="card-text"><i class="ion-md-time"></i> {$art.create_time|showdate}&nbsp;&nbsp;<i class="ion-md-paper-plane"></i> {$art.views}</p>
                                    </a>
                                  </div>
                                </div>
                            {/volist}
                        </div>
                        </div>
                        {$page|raw}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}