<div class="col-lg-3 sidecolumn">
    {php}
        $catelist=$categories[$channel['id']];
    {/php}
    <div class="card">
        <div class="card-header">
            {$channel.title}
        </div>
        <div class="card-body">
            <div class="list-group">
                {Volist name="catelist" id="c"}
                    <a class="list-group-item {$c['id']==$category['id']?'active':''}" title="{$c.title}" href="{:listurl($c['name'], $channel['name'])}">{$c.title}</a>
                {/Volist}
            </div>
        </div>
    </div>
    <div class="card text-center">
        {extendtag:advs var="advs" flag="rightad" /}
        {volist name="advs" id="ad"}
            <a href="{$ad.url}" target="_blank" ref="no_follow"><img src="{$ad.image}" title="{$ad.title}"/></a>
        {/volist}
    </div>
</div>