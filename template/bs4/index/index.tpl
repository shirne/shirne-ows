{extend name="public:base"/}
{block name="body"}
<div class="index-body">
    {extendtag:advs var="banners" flag="banner"/}
    <div id="carouselExampleIndicators" class="carousel index-carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            {volist name="banners" id="item" key="k"}
        <li data-target="#carouselExampleIndicators" data-slide-to="{$k}" {if $k == 1}class="active"{/if}></li>
        {/volist}
        </ol>
        <div class="carousel-inner">
            {volist name="banners" id="item" key="k"}
        <div class="carousel-item{$k == 1 ? ' active':''}" style="background-image:url({$item.image});height:100%;">
            <img class="d-block bg-image w-100" src="{$item.image}" alt="{$image.title}">
        </div>
        {/volist}
        </div>
        <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators"  data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators"  data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
        </button>
    </div>
</div>
{/block}
{block name="script"}

{/block}
