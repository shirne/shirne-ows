{extend name="public:base"/}
{block name="body"}
{/block}
{block name="script"}
<div class="main">
<div class="article-body shadow">
    <h1 class="article-title">{$article.title}</h1>
    <div class="article-info text-muted text-center">
        <span class="ml-2"><i class="ion-md-calendar"></i> {$article.create_time|showdate}</span>
    </div>
    <div class="article-content">
        <div>
            {$article.content|raw}
        </div>
    </div>
</div>
</div>
{/block}