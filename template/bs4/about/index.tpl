{extend name="public:base" /}
{block name="header"}
    <link href="__STATIC__/ueditor/third-party/SyntaxHighlighter/shCoreDefault.css" rel="stylesheet">
{/block}
{block name="body"}
    <div class="main">
        {include file="channel:_banner" /}

        <div class="breadcrumb-box">
			<div class="container">
				<nav aria-label="breadcrumb" >
					<ol class="breadcrumb">
						<li class="breadcrumb-item" ><i class="ion-md-pin" name="breadcrumb"></i> 首页</li>
						{volist name="categoryTree" id="c"}
                            <li class="breadcrumb-item" >{$c['title']}</li>
                        {/volist}
					</ol>
				  </nav>
			</div>
		</div>

        
        <div class="container">
			<div class="row">
				{include file="channel:_side" /}
                
				<div class="col">
                    <div class="article-body">
                        <h1 class="article-title">{$article.title}</h1>
                        {if !empty($images)}
                            <div class="article-slides">
                                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        {volist name="images" id="img"}
                                        <li data-target="#carouselExampleCaptions" data-slide-to="{$i-1}">
                                        </li>
                                        {/volist}
                                    </ol>
                                    <div class="carousel-inner">
                                        {volist name="images" id="img"}
                                            <div class="carousel-item" style="background-image:url({$img.image})">
                                                <img src="{$img.image}" alt="{$img.title}">
                                                <div class="carousel-caption d-none d-md-block">
                                                    <h5>{$img.title}</h5>
                                                    <p>{$img.title}</p>
                                                </div>
                                            </div>
                                        {/volist}
                                    </div>
                                </div>
                            </div>
                        {/if}
                        <div class="article-content">
                            <div>
                                {$article.content|raw}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}