<footer class="bd-footer text-muted">
    <div class="container p-3 p-md-5">
      <ul class="list-unstyled bd-footer-links clearfix">
        {volist name="navigator" id="nav"}
        <li><a class="bnav-link" href="{$nav['url']}" target="{$nav['target']}">{$nav['title']}</a></li>
        {/volist}
      </ul>
      <div class="text-muted">
        版权所有&copy;{:date('Y')} {$config['site-name']} ALL RIGHTS RESERVED<br />
        电话：{$config['site-telephone']}E-mail：{$config['site-email']}<br />
        地址：{$config['site-address']}<br />
        <a href="https://beian.miit.gov.cn/" target="_blank">{$config['site-icp']}</a>{$config['site-tongji']|raw}&nbsp;
        {if !empty($config['gongan-icp'])}
            {php}$icpcode=preg_replace('/[^\d]+/','',$config['gongan-icp']){/php}
            <a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode={$icpcode}" target="_blank"> <img src="__STATIC__/images/beianicon.png" style="vertical-align: middle;" /> {$config['gongan-icp']}</a>
        {/if}
      </div>
    </div>
</footer>