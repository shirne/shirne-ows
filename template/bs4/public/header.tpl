<header class="sticky-top bg-primary">
    <div class="container">
    <nav class="navbar navbar-dark bg-primary justify-content-between navbar-expand-lg nav-box">
            <a class="navbar-brand float-left" href="/" style="background-image:url({$config['site-weblogo']})"><img src="{$config['site-weblogo']}" alt="{$config['site-name']}"></a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#bs-navbar-collapse" aria-expanded="false">
                <span class="ion-md-menu navbar-toggler-icon"></span>
            </button>

            <div class="collapse main-nav navbar-collapse justify-content-end" id="bs-navbar-collapse">
                <ul class="navbar-nav">
                    {volist name="navigator" id="nav"}
                        {if empty($nav['subnav'])}
                            <li class="nav-item" data-model="{$nav['model']}"><a class="nav-link" href="{$nav['url']}" target="{$nav['target']}">{$nav['title']}</a></li>
                            {else/}
                            <li class="nav-item dropdown" data-model="{$nav['model']}">
                                <a href="{$nav['url']}" target="{$nav['target']}" class="nav-link dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">{$nav['title']} <span class="caret"></span></a>
                                <div class="dropdown-menu">
                                    {volist name="nav['subnav']" id="nav"}
                                        <a class="dropdown-item" target="{$nav['target']}" href="{$nav['url']}">{$nav['title']}</a>
                                    {/volist}
                                </div>
                            </li>
                        {/if}
                    {/volist}
                </ul>
                <div class="nav-bg"></div>
            </div>
    </nav>
    </div>
</header>
<script>
    jQuery(function($){
        var hTimeout = 0;
        $(window).scroll(function () {
            clearTimeout(hTimeout)
            hTimeout = setTimeout(function(){
                var changed=false;
                if( $(window).scrollTop()>150){
                    $('header').addClass('header-collapse');
                    changed=true;
                }else if( $(window).scrollTop()<100){
                    $('header').removeClass('header-collapse');
                    changed=true;
                }
                setTimeout(function(){
                    $('.main-nav').trigger('mouseout')
                },500);
            }, 100);
        }).trigger('scroll');
    })
</script>