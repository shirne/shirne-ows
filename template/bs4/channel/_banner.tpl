{php}if(empty($bannerflag)){ $bannerflag = 'channel_'.$channel['id']; }{/php}
{extendtag:advs var="banners" flag="$bannerflag"/}
<div id="carouselExampleIndicators" class="carousel channel-carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        {volist name="banners" id="item" key="k"}
      <li data-target="#carouselExampleIndicators" data-slide-to="{$k}" {if $k == 0}class="active"{/if}></li>
      {/volist}
    </ol>
    <div class="carousel-inner">
        {volist name="banners" id="item" key="k"}
      <div class="carousel-item{$k == 0 ? ' active':''}" style="background-image:url({$item.image});height:100%;">
        <img class="d-block w-100" src="{$item.image}" alt="{$image.title}">
      </div>
      {/volist}
    </div>
    <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators"  data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators"  data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </button>
</div>