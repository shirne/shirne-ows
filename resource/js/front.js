function setNav(nav, navbox) {
    if(!navbox)navbox = $('.main-nav');
    if(!(navbox instanceof jQuery)) navbox = $(navbox);
    var current=findCurrentNav(nav,navbox);
    setNavHover(current,navbox);

    var intval=0;
    navbox.hover(function(){
        clearTimeout(intval)
    },function(){
        intval = setTimeout(function(){
            setNavHover(current,navbox);
        },500)
    })
    navbox.find('.nav-item').mouseenter(function(){
        var index = navbox.find('.nav-item').index(this);
        setNavHover(index,navbox);
    })
}

function setNavHover(index,navbox){
    var items=navbox.find('.nav-item');
    items.removeClass('active prev-hover')
    items.eq(index).addClass('active');
    navbox.find('.nav-item.active').prev().addClass('prev-hover');

    var dataPad = navbox.find('.nav-bg').data('padding') || 0;
    navbox.find('.nav-bg').css('width',items.eq(index).outerWidth() - dataPad*2)
        .css('right',(items.length-index-1)*items.eq(0).outerWidth() + dataPad)
}

function findCurrentNav(nav,navbox){
    var items=navbox.find('.nav-item');
    
    for(var i=0;i<items.length;i++){
        if(items.eq(i).data('model')===nav){
            return i;
        }
    }
    var pnav=nav.substr(0,nav.lastIndexOf('-'));
    if(pnav == nav || !pnav){
        return 0;
    }
    
    return findCurrentNav(pnav,navbox);
}

jQuery(function($){
    if($(window).width()>=991){
        $('.main-nav>.dropdown').hover(
            function () {
                $(this).find('.dropdown-menu').stop(true,false).slideDown();
            },
            function () {
                $(this).find('.dropdown-menu').stop(true,false).slideUp();
            }
        );
    }else{
        $('.main-nav>.dropdown>.dropdown-toggle').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            var opened=$(this).data('opened');
            var p = $(this).parents('.dropdown');
            if(opened){
                p.find('.dropdown-menu').stop(true, false).slideUp();
            }else {
                p.siblings().children('.dropdown-menu').stop(true, false).slideUp();
                p.siblings().children('.dropdown-toggle').data('opened',false);
                p.find('.dropdown-menu').stop(true, false).slideDown();
            }
            $(this).data('opened',!opened);
        })
    }
});