<?php


namespace app\common\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;

/**
 * Class Addon
 * @package app\common\command
 */
class Addon extends Command
{
    protected function configure()
    {
        $this->setName('addon')
            ->setDescription('addon scaffold');
    }

    protected function execute(Input $input, Output $output)
    {
        
    }
}