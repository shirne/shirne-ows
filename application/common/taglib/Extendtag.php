<?php

namespace app\common\taglib;

use app\common\core\BaseTabLib;

/**
 * Class Extendtag
 * @package app\common\taglib
 */
class Extendtag extends BaseTabLib
{
    protected $tags =[
        'keywords'=>['attr'=>'var,group,limit','close'=>0],
        'links'=>['attr'=>'var,group,limit','close'=>0],
        'adposition'=>['attr'=>'var,flag,platform,limit','close'=>0],
        'advs'=>['attr'=>'var,flag,platform,limit','close'=>0],
        'aditem'=>['attr'=>'var,flag','close'=>0],
        'booth'=>['attr'=>'var,flag','close'=>0],
        'notices'=>['attr'=>'var,limit','close'=>0],
        'notice'=>['attr'=>'var,name','close'=>0],
        'feedback'=>['attr'=>'var,limit,page,type','close'=>0],
        'feedcount'=>['attr'=>'var,type,status,start_time,end_time','close'=>0]
    ];

    public function tagKeywords($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'keywords';
        $group = isset($tag['group']) ? $this->parseArg($tag['group']) : '';

        $parseStr='<?php ';
        if(empty($group)){
            $group = "''";
        }
        $parseStr.='$'.$var.'=\app\common\model\KeywordsModel::getKeywords('.$group;
        
        if(!empty($tag['limit'])){
            $parseStr .= ','.intval($tag['limit']);
        }
        $parseStr .= ');';

        $parseStr .= ' ?>';
        return $parseStr;
    }

    public function tagLinks($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'links';
        $group = isset($tag['group']) ? $this->parseArg($tag['group']) : '';

        $parseStr='<?php ';

        $parseStr.='$'.$var.'=\think\Db::name("Links")';
        if(!empty($group)){
            $parseStr .= '->where(\'group\','.$group.')';
        }
        $parseStr .= '->order("sort ASC,id ASC")';
        if(!empty($tag['limit'])){
            $parseStr .= '->limit('.intval($tag['limit']).')';
        }
        $parseStr .= '->select();';

        $parseStr .= ' ?>';
        return $parseStr;
    }

    public function tagBooth($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'booth';

        $parseStr='<?php ';

        $parseStr.='$'.$var.'=\app\common\model\BoothModel::fetchBooth('.$this->parseArg($tag['flag']).', true);';

        $parseStr .= ' ?>';
        return $parseStr;
    }

    public function tagAdposition($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'adposition';
        $limit=empty($tag['limit'])?'':', '.intval($tag['limit']);
        $platform = isset($tag['platform']) ? $this->parseArg($tag['platform']) : '';
        if(!empty($platform)){
            $platform = ','.$platform;
        }else{
            $platform = ',\'\'';
        }

        $parseStr='<?php ';

        $parseStr.='$'.$var.'=\app\common\model\AdvGroupModel::getAdPosition('.$this->parseArg($tag['flag']).$platform .$limit.');';

        $parseStr .= ' ?>';
        return $parseStr;
    }

    public function tagAdvs($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'advs';
        $limit=empty($tag['limit'])?'':', '.intval($tag['limit']);
        $platform = isset($tag['platform']) ? $this->parseArg($tag['platform']) : '';
        if(!empty($platform)){
            $platform = ','.$platform;
        }else{
            $platform = ',\'\'';
        }

        $parseStr='<?php ';

        $parseStr.='$'.$var.'=\app\common\model\AdvGroupModel::getAdList('.$this->parseArg($tag['flag']).$platform . $limit.');';

        $parseStr .= ' ?>';
        return $parseStr;
    }

    public function tagAditem($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'aditem';

        $parseStr='<?php ';

        $parseStr.='$'.$var.'=\app\common\model\AdvGroupModel::getAdItem('.$this->parseArg($tag['flag']) .');';

        $parseStr .= ' ?>';
        return $parseStr;
    }

    public function tagNotices($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'links';

        $parseStr='<?php ';

        $parseStr.='$'.$var.'=\think\Db::name("Notice")';
        $parseStr .= "->where('status',1)";
        $parseStr .= '->order("create_time DESC")';
        if(!empty($tag['limit'])){
            $parseStr .= '->limit('.intval($tag['limit']).')';
        }
        $parseStr .= '->select();';

        $parseStr .= ' ?>';
        return $parseStr;
    }

    public function tagNotice($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'notice_model';
        $name=isset($tag['name']) ? $this->parseArg($tag['name']) : '';

        $parseStr='<?php ';

        $parseStr.='$'.$var.'=\think\Db::name("Notice")';
        $parseStr .= '->where(\'status\',1)';
        if(!empty($name)){
            $parseStr .= '->where(\'page\','.$name.')';
        }
        $parseStr .= '->find();';

        $parseStr .= ' ?>';
        return $parseStr;
    }

    public function tagFeedback($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'feedbacks';

        $parseStr='<?php ';

        $parseStr.='$'.$var.'=\think\Db::view("Feedback","*")';
        $parseStr .= '->view("member",["username","nickname","avatar"],"Feedback.member_id=member.id","LEFT")';
        $parseStr .= '->view("manager",["realname"=>"manager_name"],"Feedback.manager_id=manager.id","LEFT")';
        if(isset($tag['type'])){
            if(strpos($tag['type'],',')>0){
                $tag['type'] = idArr($tag['type']);
                $parseStr .= '->whereIn("Feedback.type",['.implode(', ',$tag['type']).'])';
            }else{
                $tag['type'] = intval($tag['type']);
                $parseStr .= '->where("Feedback.type",'.$tag['type'].')';
            }
        }
        $parseStr .= '->where("Feedback.status",1)';
        $parseStr .= '->order("Feedback.create_time DESC")';

        if($tag['page']=='1'){
            $parseStr .= '->paginate(' . intval($tag['limit']) . ');';
        }else {
            if (!empty($tag['limit'])) {
                $parseStr .= '->limit(' . intval($tag['limit']) . ')';
            }
            $parseStr .= '->select();';
        }
        $parseStr .= ' ?>';
        return $parseStr;
    }

    public function tagFeedcount($tag){
        $var  = isset($tag['var']) ? $tag['var'] : 'feedcount';

        $parseStr='<?php ';

        $parseStr.='$'.$var.'=\think\Db::name("Feedback")';
        if(isset($tag['type'])){
            if(strpos($tag['type'],',')>0){
                $tag['type'] = idArr($tag['type']);
                $parseStr .= '->whereIn("type",['.implode(', ',$tag['type']).'])';
            }else{
                $tag['type'] = intval($tag['type']);
                $parseStr .= '->where("type",'.$tag['type'].')';
            }
        }
        if(isset($tag['start_time'])){
            $parseStr .= '->where("create_time", ">=",'.intval(strtotime($tag['start_time'])).')';
        }
        if(isset($tag['end_time'])){
            $parseStr .= '->where("create_time", "<=",'.intval(strtotime($tag['start_time'])).')';
        }
        if(isset($tag['status'])){
            $parseStr .= '->where("status", '.intval($tag['status']).')';
        }else{
            $parseStr .= '->where("status",1)';
        }
        
        $parseStr .= '->count()';

        $parseStr .= ' ?>';
        return $parseStr;
    }
}