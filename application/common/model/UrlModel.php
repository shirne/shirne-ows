<?php

namespace app\common\model;

use app\common\core\CacheableModel;
use think\Db;

/**
 * 网址中转模块
 * Class UrlModel
 * @package app\common\model
 */
class UrlModel extends CacheableModel
{
    protected $pk="id";

    protected static $cacheFlip;

    public static function init()
    {
        parent::init();
    }
    
    protected function get_cache_data()
    {
        $urls=static::order('id ASC')->select()->toArray();
        return array_column($urls,'short','full');
    }

    public static function shortUrl($url){
        $urls = static::getCacheData();
        return empty($urls[$url]) ? $url : $urls[$url];
    }
    public static function fullUrl($url){
        if(static::$cacheFlip == null){
            $urls = static::getCacheData();
            static::$cacheFlip = array_flip($urls);
        }
        return empty(static::$cacheFlip[$url]) ? $url : static::$cacheFlip[$url];
    }

}