<?php

namespace app\common\model;


use app\common\core\BaseModel;
use think\Db;
use think\facade\Log;

class ArticleFieldsModel extends BaseModel
{
    protected $autoWriteTimestamp = true;
    protected $type = ['data_options'=>'array'];

    public static function init(){
        self::afterInsert(function($model){
            Db::execute('ALTER TABLE `'.config('database.prefix').'article` ADD '.$model->packSQL());
        });
        self::afterUpdate(function($model){
            $origin = $model->getOrigin();
            $origSQL = $model->packSQL($origin);
            $newSQL = $model->packSQL();
            if($origSQL != $newSQL){
                Db::execute('ALTER TABLE `'.config('database.prefix').'article` CHANGE COLUMN `'.$origin['name'].'` '.$newSQL);
            }
        });
    }

    public static function getFields($islist = false){
        $model = static::order('sort ASC,id ASC')->field(['name','title']);
        if($islist){
            $model->where('is_list',1);
        }
        return $model->select();
    }
    public static function getSelectedFields($fields){
        if(!is_array($fields))$fields = array_map('trim',explode(',',$fields));
        $result = static::order('sort ASC,id ASC')->whereIn('name', $fields)->select()->toArray();
        $result = array_column($result, NULL, 'name');
        $return = [];
        foreach($fields as $field){
            $options = $result[$field]['data_options'];
            if(!empty($options)){
                $options = array_combine($options['keys'], $options['values']);
            }
            $result[$field]['data_options'] = $options;
            $return[] = $result[$field];
        }
        return $return;
    }

    public function packSQL($data = []){
        if(empty($data))$data = $this->getData();

        $sql = '`'.$data['name'].'`';

        if($data['data_length'] < 1){
            $data['data_length'] = 100;
        }
        $default = $data['data_value']??'';
        switch($data['field_type']){
            case 'text':
                $sql .= ' varchar('.$data['data_length'].')';
                break;
            case 'multi-text':
            case 'editor':
                $sql .= ' text';
                $default = NULL;
                break;
            case 'image':
                $sql .= ' varchar(150)';
                break;
            case 'number':
                if($default === '' || !is_numeric($default))$default = 0;
                switch($data['data_type']){
                    case '1':
                        $sql .= ' decimal(10,1)';
                        break;
                    case '2':
                        $sql .= ' decimal(10,2)';
                        break;
                    case '3':
                        $sql .= ' decimal(10,3)';
                        break;
                    case '4':
                        $sql .= ' decimal(10,4)';
                        break;
                    default:
                    $sql .= ' int(11)';
                    break;
                }
                break;
            case 'single':
                $sql .= ' tinyint(4)';
                break;
            case 'select':
            case 'radio':
                $sql .= ' varchar(20)';
                break;
            case 'checkbox':
                $sql .= ' varchar(200)';
                break;
            default:
            $sql .= ' varchar('.$data['data_length'].')';
            break;
        }
        if($default !== NULL){
            $sql .= ' default \''.$default.'\'';
        }

        return $sql;
    }

    public static function writeTransData($data){
        if(empty($data)){
            return $data;
        }
        if($data instanceof BaseModel){
            $keys = array_keys($data->getData());
        }else{
            $keys = array_keys($data);
        }
        
        $fields = static::whereIn('name',$keys)->where('field_type','checkbox')->select();
        foreach($fields as $field){
            $data[$field['name']]=json_encode($data[$field['name']]??[]);
        }
        return $data;
    }

    public static function readTransData($data){
        if(empty($data)){
            return $data;
        }
        if($data instanceof BaseModel){
            $keys = array_keys($data->getData());
        }else{
            $keys = array_keys($data);
        }
        
        $fields = static::whereIn('name',$keys)->where('field_type','checkbox')->select();
        foreach($fields as $field){
            if(is_array($data[$field['name']]))break;
            $data[$field['name']]=force_json_decode($data[$field['name']]??'');
        }
        return $data;
    }
}