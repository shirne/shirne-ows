{extend name="public:base" /}

{block name="body"}

{include file="channel/_bread" menu="adv_index" title="字段管理" /}

<div id="page-wrapper" class="container-fluid">
    <div class="page-header">{$id>0?'编辑':'添加'}字段</div>
    <div class="page-content">
    <form method="post" class="page-form" action="">
        <div class="form-group">
            <label for="title">字段标题</label>
            <input type="text" name="title" class="form-control" value="{$model.title}" placeholder="字段标题">
        </div>
        <div class="form-group">
            <label for="name">字段名称</label>
            <input type="text" name="name" class="form-control" value="{$model.name}" placeholder="字段名称">
        </div>
        <div class="form-row mb-2">
            <label class="col-2" for="type">字段类型</label>
            <div class="col-3">
                <select name="field_type" class="form-control">
                    <option value="text">单行文本</option>
                    <option value="multi-text" {$model['field_type']=='multi-text'?'selected':''}>多行文本</option>
                    <option value="editor" {$model['field_type']=='editor'?'selected':''}>编辑器</option>
                    <option value="image" {$model['field_type']=='image'?'selected':''}>图片</option>
                    <option value="number" {$model['field_type']=='number'?'selected':''}>数字</option>
                    <option value="single" {$model['field_type']=='single'?'selected':''}>状态开关</option>
                    <option value="radio" {$model['field_type']=='radio'?'selected':''}>多项单选</option>
                    <option value="select" {$model['field_type']=='select'?'selected':''}>下拉选择</option>
                    <option value="checkbox" {$model['field_type']=='checkbox'?'selected':''}>多项多选</option>
                </select>
            </div>
            <div class="col-3 dataext datalength">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">字段长度</span></div>
                    <input type="text" name="data_length" class="form-control" value="{$model.data_length|default=100}" placeholder="字段长度">
                </div>
            </div>
            <div class="col-3 dataext datatype">
                <div class="input-group">
                    <div class="input-group-prepend"><span class="input-group-text">数值类型</span></div>
                    <select name="data_type" class="form-control">
                        <option value="0">整数</option>
                        <option value="1">一位小数</option>
                        <option value="2">两位小数</option>
                        <option value="3">三位小数</option>
                        <option value="4">四位小数</option>
                    </select>
                </div>
            </div>
            <div class="col"></div>
        </div>
        <div class="form-group dataext dataoption">
            <label class="pl-2 mr-2">选项数据</label>
            <div class="form-group col">
                <div class="prop-groups">
                    {foreach $model['data_options'] as $k => $prop}
                        <div class="input-group mb-2" >
                            <input type="text" class="form-control" placeholder="选项值" style="max-width:120px;" name="data_options[keys][]" value="{$k}"/>
                            <input type="text" class="form-control" placeholder="显示文本" name="data_options[values][]" value="{$prop}"/>
                            <div class="input-group-append delete"><a href="javascript:" class="btn btn-outline-secondary"><i class="ion-md-trash"></i> </a> </div>
                        </div>
                    {/foreach}
                </div>
                <a href="javascript:" class="btn btn-outline-dark btn-sm addpropbtn"><i class="ion-md-add"></i> 添加属性</a>
            </div>
        </div>
        <div class="form-group">
            <label for="name">是否HTML</label>
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-outline-secondary {$model['is_html']?'':' active'}">
                  <input type="radio" name="is_html" value="0"  {$model['is_html']?'':' checked'}> 否
                </label>
                <label class="btn btn-outline-primary {$model['is_required']?' active':''}">
                  <input type="radio" name="is_html" value="1" {$model['is_html']?' checked':''}> 是
                </label>
              </div>
        </div>
        <div class="form-group">
            <label for="name">是否必填</label>
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-outline-secondary {$model['is_required']?'':' active'}">
                  <input type="radio" name="is_required" value="0"  {$model['is_required']?'':' checked'}> 否
                </label>
                <label class="btn btn-outline-primary {$model['is_required']?' active':''}">
                  <input type="radio" name="is_required" value="1" {$model['is_required']?' checked':''}> 是
                </label>
              </div>
        </div>
        <div class="form-group">
            <label for="data_value">默认值</label>
            <input type="text" name="data_value" class="form-control" value="{$model.data_value}" placeholder="字段名称">
        </div>
        <div class="form-group">
            <label for="description">字段说明</label>
            <input type="text" name="description" class="form-control" value="{$model.description}" placeholder="字段名称">
        </div>
        <div class="form-group">
            <label for="sort">排序</label>
            <input type="text" name="sort" class="form-control" value="{$model.sort|default=9}" placeholder="字段名称">
        </div>
        <div class="form-group submit-btn">
            <input type="hidden" name="id" value="{$model.id}">
            <button type="submit" class="btn btn-primary">{$id>0?'保存':'添加'}</button>
        </div>
    </form>
    </div>
</div>
{/block}

{block name="script"}
    <script type="text/javascript">
        jQuery(function ($) {
            function addProp(data){
                if(!data)data={key:'',value:''};
                $('.prop-groups').append('<div class="input-group mb-2" >\n' +
                    '                            <input type="text" class="form-control prop-key" placeholder="选项值" style="max-width:120px;" name="data_options[keys][]" value="'+data.key+'"/>\n' +
                    '                            <input type="text" class="form-control prop-value" placeholder="显示文本" name="data_options[values][]"  value="'+data.value+'"/>\n' +
                    '                            <div class="input-group-append delete"><a href="javascript:" class="btn btn-outline-secondary"><i class="ion-md-trash"></i> </a> </div>\n' +
                    '                        </div>');
            }
            $('.addpropbtn').click(function (e) {
                addProp();
            });
            $('.prop-groups').on('click','.delete .btn',function (e) {
                var self=$(this);
                dialog.confirm('确定删除该字段？',function () {
                    self.parents('.input-group').remove();
                })
            });
            $('[name=field_type]').change(function(e){
                var value=$(this).val()
                $('.dataext').hide();
                var dft = $('[name=data_value]').val()
                switch(value){
                    case 'text':
                    $('.datalength').show()
                        break;
                    case 'multi-text':
                    break;
                    case 'editor':
                    break;
                    case 'image':
                    break;
                    case 'number':
                        if(isNaN(dft))$('[name=data_value]').val('0')
                        $('.datatype').show();
                    break;
                    case 'single':
                    case 'select':
                    case 'radio':
                    case 'checkbox':
                        $('.dataoption').show();
                        $('.prop-groups .prop-key').prop('readonly',false)
                    break;
                }
                if(value == 'single'){
                    $('.prop-groups').html('');
                    addProp({key:1,value:'开启'});
                    addProp({key:0,value:'关闭'});
                    $('.prop-groups .prop-key').prop('readonly',true)
                }
            }).trigger('change');
        });
    </script>
{/block}
