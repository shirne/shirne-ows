{extend name="public:base" /}

{block name="body"}
    {include file="channel/_bread" title="内容详情"/}
<div id="page-wrapper" class="container-fluid">
    <div class="page-header" style="border:0;">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li><span class="nav-link">{$id>0?'编辑':'添加'}{$channel.short}</span></li>
            <li class="nav-item" role="presentation">
              <a class="nav-link active" id="base-tab" data-toggle="tab" href="#contbase" role="tab" aria-controls="contbase" aria-selected="true">基本信息</a>
            </li>
            <li class="nav-item" role="presentation">
              <a class="nav-link" id="ext-tab" data-toggle="tab" href="#context" role="tab" aria-controls="context" aria-selected="false">扩展信息</a>
            </li>
          </ul>
    </div>
    <div id="page-content" >
    <form method="post" class="page-form" action="" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="contbase" role="tabpanel" aria-labelledby="base-tab">
            <div class="form-row">
                <div class="col form-group">
                    <label for="article-title">{$channel.short}标题</label>
                    <input type="text" name="title" class="form-control" value="{$article.title}" id="article-title" placeholder="输入{$channel.short}标题">
                </div>
                <div class="col form-group">
                    <label for="article-cate">{$channel.short}分类</label>
                    <select name="cate_id" id="article-cate" class="form-control">
                        <option value="{$channel.id}" data-pid="{$channel['pid']}" data-name="{$channel['name']}" {$article['cate_id'] == $channel['id']?'selected="selected"':""} data-props="{$channel['props']}">{$channel.title}</option>
                        {foreach $category as $key=>$v}
                            <option value="{$v.id}" data-pid="{$v['pid']}" data-name="{$v['name']}" {$article['cate_id'] == $v['id']?'selected="selected"':""} data-props="{$v['props']}">{$v.html} {$v.title}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col form-group">
                    <label for="image">封面图</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="upload_cover"/>
                            <label class="custom-file-label" for="upload_cover">选择文件</label>
                        </div>
                    </div>
                    {if $article['cover']}
                        <figure class="figure">
                            <img src="{$article.cover}" class="figure-img img-fluid rounded" alt="image">
                            <figcaption class="figure-caption text-center">{$article.cover}</figcaption>
                        </figure>
                        <input type="hidden" name="delete_cover" value="{$article.cover}"/>
                    {/if}
                </div>
                <div class="col form-group">
                    <label for="article-cate">文章版权</label>
                    <select name="copyright_id" class="form-control">
                        <option value="0">无</option>
                        {foreach $copyrights as $key=>$v}
                            <option value="{$v.id}" {$article['copyright_id'] == $v['id']?'selected="selected"':""} >{$v.title}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label for="description">{$channel.short}摘要</label>
                <textarea name="description" class="form-control" >{$article.description}</textarea>
            </div>
            <div class="form-group">
                <label for="article-content">{$channel.short}内容</label>
                <script id="article-content" name="content" type="text/plain">{$article.content|raw}</script>
            </div>
        </div>
        <div class="tab-pane fade" id="context" role="tabpanel" aria-labelledby="ext-tab">
            <div class="form-row">
                <div class="col form-group">
                    <label for="vice_title">副标题</label>
                    <input type="text" name="vice_title" class="form-control" value="{$article.vice_title}" >
                </div>
                <div class="col form-group">
                    <label for="name">URL名称</label>
                    <div class="input-group">
                        <span class="input-group-prepend"><span class="input-group-text urlprefix" data-urltpl="{:listurl('__CATE__',$channel['name'])}/">/</span></span>
                        <input type="text" name="name" class="form-control" value="{$article.name}" placeholder="留空系统自动生成" >
                        <span class="input-group-append"><span class="input-group-text">.html</span></span>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col form-group">
                    <label for="create_time">发布时间</label>
                    <input type="text" name="create_time" class="form-control datepicker" data-format="YYYY-MM-DD hh:mm:ss" value="{$article.create_time|showdate}" placeholder="默认取当前系统时间" >
                </div>
                <div class="col form-group">
                    <label for="template">模板文件</label>
                    <input type="text" name="template" class="form-control" placeholder="留空使用默认页面" value="{$article.template}" >
                </div>
                <div class="col form-group">
                    <label for="template">排序</label>
                    <input type="text" name="sort" class="form-control" value="{$article.sort}" >
                </div>
            </div>
            <div class="form-row align-items-baseline">
                <label class="pl-2 mr-2">自定义字段</label>
                <div class="form-group col">
                    <div class="prop-groups">
                        {foreach $article['prop_data']??[] as $k => $prop}
                            <div class="input-group mb-2" >
                                <input type="text" class="form-control" style="max-width:120px;" name="prop_data[keys][]" value="{$k}"/>
                                <input type="text" class="form-control" name="prop_data[values][]" value="{$prop}"/>
                                <div class="input-group-append delete"><a href="javascript:" class="btn btn-outline-secondary"><i class="ion-md-trash"></i> </a> </div>
                            </div>
                        {/foreach}
                    </div>
                    <a href="javascript:" class="btn btn-outline-dark btn-sm addpropbtn"><i class="ion-md-add"></i> 添加属性</a>
                </div>
            </div>
            <div class="form-row align-items-center">
                <label class="pl-2 mr-2">{$channel.short}类型</label>
                <div class="form-group col">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons" >
                        {volist name="types" id="type" key="k"}
                            <label class="btn btn-outline-secondary{$key==($article['type'] & $key)?' active':''}">
                                <input type="checkbox" name="type[]" value="{$key}" autocomplete="off" {$key==($article['type'] & $key)?'checked':''}>{$type}
                            </label>
                        {/volist}
                    </div>
                </div>
                <label class="pl-2 mr-2">浏览量</label>
                <div class="form-group col">
                    <div class="input-group">
                        <input type="text" class="form-control" readonly value="{$article['views']}" />
                        <span class="input-group-middle"><span class="input-group-text">+</span></span>
                        <input type="text" class="form-control" name="v_views" title="虚拟浏览量" value="{$article['v_views']}" />
                    </div>
                </div>
                <label class="pl-2 mr-2">点赞数</label>
                <div class="form-group col">
                    <div class="input-group">
                        <input type="text" class="form-control" readonly value="{$article['digg']}" />
                        <span class="input-group-middle"><span class="input-group-text">+</span></span>
                        <input type="text" class="form-control" name="v_digg" title="虚拟点赞数" value="{$article['v_digg']}" />
                    </div>
                </div>
                {if $channel['is_comment']}
                <label class="pl-2 mr-2">关评论</label>
                <div class="form-group col">
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-outline-primary{$article['close_comment']==1?' active':''}">
                            <input type="radio" name="close_comment" value="1" autocomplete="off" {$article['close_comment']==1?' checked':''}> 关闭评论
                        </label>
                        <label class="btn btn-outline-secondary{$article['close_comment']==0?' active':''}">
                            <input type="radio" name="close_comment" value="0" autocomplete="off"{$article['close_comment']==0?' checked':''}> 允许评论
                        </label>
                    </div>
                </div>
                {/if}
            </div>

            {volist name="fields" id="item"}
                <div class="form-group">
                    <label for="{$item.name}" >{$item.title}</label>
                    <div >
                        {switch name="item.field_type"}
                            {case value="text"}
                                <input type="text" class="form-control" name="{$item.name}" value="{$article[$item['name']]}" placeholder="{$item.value}">
                            {/case}
                            {case value="image"}
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="upload_{$item.name}"/>
                                        <label class="custom-file-label" for="upload_{$item.name}">选择文件</label>
                                    </div>
                                </div>
                                {if !empty($article[$item['name']])}
                                    <figure class="figure">
                                        <img src="{$article[$item['name']]}" class="figure-img img-fluid rounded" alt="image">
                                        <figcaption class="figure-caption text-center">{$article[$item['name']]}</figcaption>
                                    </figure>
                                    <input type="hidden" name="delete_{$item.name}" value="{$article[$item['name']]}"/>
                                {/if}
                            {/case}
                            {case value="location"}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="{$item.name}" value="{$article[$item['name']]}" placeholder="{$item.description}">
                                    <div class="input-group-append">
                                        <a href="javascript:" class="btn btn-outline-secondary locationPick">选择位置</a>
                                    </div>
                                </div>
                            {/case}
                            {case value="number"}
                                <input type="number" class="form-control" name="{$item.name}" value="{$article[$item['name']]}" placeholder="{$item.description}">
                            {/case}
                            {case value="single"}
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                {foreach $item.data as $k => $value}
                                    {if $article[$item['name']]==$k}
                                        <label class="btn btn-outline-secondary active">
                                            <input type="radio" name="{$item.name}" value="{$k}" autocomplete="off" checked> {$value}
                                        </label>
                                    {else /}
                                        <label class="btn btn-outline-secondary">
                                            <input type="radio" name="{$item.name}" value="{$k}" autocomplete="off"> {$value}
                                        </label>
                                    {/if}
                                {/foreach}
                                </div>
                            {/case}
                            {case value="radio"}
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                {foreach $item.data_options as $k => $value}
                                    {if $article[$item['name']]==$k}
                                        <label class="btn btn-outline-secondary active">
                                            <input type="radio" name="{$item.name}" value="{$k}" autocomplete="off" checked> {$value}
                                        </label>
                                    {else /}
                                        <label class="btn btn-outline-secondary">
                                            <input type="radio" name="{$item.name}" value="{$k}" autocomplete="off"> {$value}
                                        </label>
                                    {/if}
                                {/foreach}
                                </div>
                            {/case}
                            {case value="checkbox"}
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                {foreach $item.data_options as $k => $value}
                                    {if in_array($k,$article[$item['name']])}
                                        <label class="btn btn-outline-secondary active">
                                            <input type="radio" name="{$item.name}[]" value="{$k}" autocomplete="off" checked> {$value}
                                        </label>
                                    {else /}
                                        <label class="btn btn-outline-secondary">
                                            <input type="radio" name="{$item.name}[]" value="{$k}" autocomplete="off"> {$value}
                                        </label>
                                    {/if}
                                {/foreach}
                                </div>
                            {/case}
                            {case value="select"}
                                <select name="{$item.name}" class="form-control">
                                    {foreach $item.data_options as $k => $value}
                                        {if $k==$article[$item['name']]}
                                            <option value="{$k}" selected="selected">{$value}</option>
                                        {else/}
                                            <option value="{$k}">{$value}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                            {/case}
                            {case value="textarea"}
                                {if $item['is_html']}
                                    <textarea name="{$item.name}" class="form-control" placeholder="{$item.description}">{$article[$item['name']]|raw}</textarea>
                                    {else/}
                                    <textarea name="{$item.name}" class="form-control" placeholder="{$item.description}">{$article[$item['name']]}</textarea>
                                {/if}
                                
                            {/case}
                            {case value="html"}
                                <textarea name="{$item.name}" id="editor-{$item.name}" class="w-100 html-content" placeholder="{$item.description}">{$article[$item['name']]|raw}</textarea>
                            {/case}
                        {/switch}
                    </div>
                </div>
            {/volist}
        </div>
    </div>
        <div class="form-group submit-btn">
            <input type="hidden" name="id" value="{$article.id}">
            <button type="submit" class="btn btn-primary">{$id>0?'保存':'添加'}</button>
        </div>
    </form>
        </div>
</div>
    {/block}
{block name="script"}

<script type="text/javascript" src="__STATIC__/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="__STATIC__/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript">
    var ue = UE.getEditor('article-content',{
        toolbars: Toolbars.normal,
        initialFrameHeight:500,
        zIndex:100
    });
    
    jQuery(function ($) {
        $('.locationPick').click(function () {
            var input=$(this).parents('.input-group').find('input[type=text]');
            var locate=null
            var locates=input.val()
            if(locates){
                locates=locates.split(',');
                locate={lng:locates[0],lat:locates[1]}
            }
            dialog.pickLocate('',function (locate) {
                console.log(locate);
                input.val(locate.lng+','+locate.lat);
            },locate);
        });
        $('.html-content').each(function (idx,item) {
            UE.getEditor($(item).attr('id'),{
                toolbars: Toolbars.simple,
                initialFrameHeight:200,
                zIndex:100
            });
        });

        $('.addpropbtn').click(function (e) {
            addProp();
        });
        $('.prop-groups').on('click','.delete .btn',function (e) {
            var self=$(this);
            dialog.confirm('确定删除该属性？',function () {
                self.parents('.input-group').remove();
            })
        });
        function addProp(key,value) {
            $('.prop-groups').append('<div class="input-group mb-2" >\n' +
                '                            <input type="text" class="form-control" style="max-width:120px;" name="prop_data[keys][]" value="'+(key?key:'')+'" />\n' +
                '                            <input type="text" class="form-control" name="prop_data[values][]" value="'+(value?value:'')+'" />\n' +
                '                            <div class="input-group-append delete"><a href="javascript:" class="btn btn-outline-secondary"><i class="ion-md-trash"></i> </a> </div>\n' +
                '                        </div>');
        }
        function mergeArray(arr, newArr){
            if(newArr && newArr.length>0){
                for(var i=0;i<newArr.length;i++){
                    if(arr.indexOf(newArr[i]) < 0){
                        arr.push(newArr[i])
                    }
                }
            }
            return arr;
        }
        function changeCategory(select,force) {
            var option=$(select).find('option:selected');
            var curProps=[];
            var props=$(option).data('props') || [];
            var pid = $(option).data('pid');
            var name = $(option).data('name');

            $('.urlprefix').each(function(){
                var tpl = $(this).data('urltpl');
                $(this).text(tpl.replace('__CATE__',name))
            });

            while(pid > 0){
                var parentnode=$(select).find('option[value='+pid+']');
                if(!parentnode || !parentnode.length)break;
                props = mergeArray(props, $(parentnode).data('props'));
                pid = $(parentnode).data('pid');
            }

            $('.prop-groups .input-group').each(function () {
                var input=$(this).find('input');
                var prop=input.val().trim();
                if(input.eq(1).val().trim()===''){
                    if(props.indexOf(prop)<0){
                        $(this).remove();
                    }else{
                        curProps.push(prop);
                    }
                }else {
                    curProps.push(prop);
                }
            });
            for(var i=0;i<props.length;i++){
                if(curProps.indexOf(props[i])<0){
                    addProp(props[i]);
                }
            }
        }
        
        $('#article-cate').change(function (e) {
            changeCategory(this);
        });
        if('add'==="{$article['id']?'':'add'}"){
            changeCategory($('#article-cate'),true);
        }else{
            var option=$('#article-cate option:selected');
            var name = $(option).data('name');

            $('.urlprefix').each(function(){
                var tpl = $(this).data('urltpl');
                $(this).text(tpl.replace('__CATE__',name))
            });
        }
    });
</script>
{/block}