{extend name="public:base" /}

{block name="body"}
    {include file="channel/_bread" menu="channel_fields" title="字段管理" /}

    <div id="page-wrapper" class="container-fluid">

        <div class="row list-header">
            <div class="col-6">
                <a href="{:url('channel/fieldsadd')}" class="btn btn-outline-primary btn-sm"><i class="ion-md-add"></i> 添加字段</a>
            </div>
            <div class="col-6">
            </div>
        </div>
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th width="50">编号</th>
                    <th>字段</th>
                    <th>标题</th>
                    <th>类型</th>
                    <th>默认值</th>
                    <th>排序</th>
                    <th width="160">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                {foreach $lists as $key=>$v}
                    <tr>
                        <td>{$v.id}</td>
                        <td>{$v.name}</td>
                        <td>{$v.title}</td>
                        <td>{$v.field_type}</td>
                        <td>{$v.data_value}</td>
                        <td>{$v.sort}</td>
                        <td class="operations">
                            <a class="btn btn-outline-primary" title="编辑"
                                href="{:url('channel/fieldsedit',array('id'=>$v['id'],'gid'=>$gid))}"><i
                                    class="ion-md-create"></i> </a>
                            <a class="btn btn-outline-danger link-confirm" title="删除"
                                data-confirm="您真的确定要删除吗？\n\n删除后将不能恢复!"
                                href="{:url('channel/fieldsdelete',array('id'=>$v['id'],'gid'=>$gid))}"><i
                                    class="ion-md-trash"></i> </a>
                        </td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
{/block}