{extend name="public:base" /}

{block name="body"}

    {include file="public/bread" menu="channel_index" title="初始化栏目" /}

    <div id="page-wrapper" class="container-fluid">

        <div class="row list-header">
            <div class="col-6">
            </div>
            <div class="col-6">
            </div>
        </div>
        <form name="cates_form" method="post">
        <table class="table table-hover table-striped" id="column-table">
            <thead>
            <tr>
                <th width="80">排序</th>
                <th>名称</th>
                <th width="80">简称</th>
                <th>目录</th>
                <th>类型</th>
                <th>独立模板</th>
                <th>图集</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            {foreach $channels as $key=>$v}
                <tr>
                    <td><input type="text" class="form-control" name="channels[{$v.id}][sort]" value="{$v.sort}"></td>
                    <td><input type="text" class="form-control" required name="channels[{$v.id}][title]" value="{$v.title}"></td>
                    <td><input type="text" class="form-control" name="channels[{$v.id}][short]" value="{$v.short}"></td>
                    <td><input type="text" class="form-control" required name="channels[{$v.id}][name]" value="{$v.name}"></td>
                    <td>
                        <select name="channels[{$v.id}][channel_mode]" class="form-control">
                            <option value="2" {$v['channel_mode']==2?'selected':''}>栏目</option>
                            <option value="0" {$v['channel_mode']==0?'selected':''}>列表</option>
                            <option value="1" {$v['channel_mode']==1?'selected':''}>单页</option>
                        </select>
                    </td>
                    <td>
                        <select name="channels[{$v.id}][use_template]" class="form-control">
                            <option value="0" {$v['use_template']==0?'selected':''}>默认模板</option>
                            <option value="1" {$v['use_template']==1?'selected':''}>独立模板</option>
                        </select>
                    </td>
                    <td>
                        <select name="channels[{$v.id}][is_images]" class="form-control">
                            <option value="0" {$v['is_images']==0?'selected':''}>无图集</option>
                            <option value="1" {$v['is_images']==1?'selected':''}>有图集</option>
                        </select>
                    </td>
                    <td class="operations">
                        <a class="btn btn-outline-danger btn-delete" data-id="{$v.id}" title="{:lang('Delete')}" data-confirm="确定删除栏目？\n将同时删除该频道下所有分类和内容!" href="javascript:" ><i class="ion-md-trash"></i> </a>
                    </td>
                </tr>
            {/foreach}
            
            </tbody>
            <tfoot>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="6"><a href="javascript:" class="btn btn-outline-primary addrow">添加栏目</a> </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <input type="submit" class="btn btn-primary" value="保存"/>
                    </td>
                </tr>
            </tfoot>
        </table>
        <div class="text-muted mt-3">
            栏目名称和目录<span class="text-danger"> 必填 </span>，目录名使用英文字符，如: <span class="text-info">news, products, case</span> 等。<br />
            所有栏目目录<span class="text-danger"> 不可重复 </span>。<br />
            目录不能使用系统保留词 <span class="text-danger">index, admin, api, vue, task, channel</span>。<br />
            非新加的栏目，如果删除会 <span class="text-danger">同时删除栏目下的所有分类及内容</span>。<br />
            栏目类型分三种，栏目类型的，包括 <span class="text-info">栏目首页，列表页，详情页</span> 三级，栏目首页需要设计师订制显示内容。<br />
            &nbsp;&nbsp;&nbsp;&nbsp;列表类型的包括<span class="text-info">列表页和详情页</span><br />
            &nbsp;&nbsp;&nbsp;&nbsp;单页类型的栏目，没有列表页，每个类目对应一个页面，页面可订制内容，也可以直接显示详情<br />
        </div>
        </form>
    </div>
{/block}
{block name="script"}
<script type="text/html" id="addTpl">
    <tr id="{@id}">
        <td><input type="text" class="form-control" name="channels[{@id}][sort]" value=""></td>
        <td><input type="text" class="form-control" required name="channels[{@id}][title]" ></td>
        <td><input type="text" class="form-control" name="channels[{@id}][short]" ></td>
        <td><input type="text" class="form-control" required name="channels[{@id}][name]" ></td>
        <td>
            <select name="channels[{@id}][channel_mode]" class="form-control">
                <option value="2" >栏目</option>
                <option value="0" >列表</option>
                <option value="1" >单页</option>
            </select>
        </td>
        <td>
            <select name="channels[{@id}][use_template]" class="form-control">
                <option value="0" >默认模板</option>
                <option value="1" >独立模板</option>
            </select>
        </td>
        <td>
            <select name="channels[{@id}][is_images]" class="form-control">
                <option value="0" >无图集</option>
                <option value="1" >有图集</option>
            </select>
        </td>
        <td class="operations">
            <a class="btn btn-outline-danger btn-delete" title="{:lang('Delete')}" data-confirm="确定删除栏目？" href="javascript:" ><i class="ion-md-trash"></i> </a>
        </td>
    </tr>
</script>
<script>
jQuery(function(){
    var maxid = 10;
    $('.addrow').click(function(){
        maxid++;
        $(this).parents('table').find('tbody').append($('#addTpl').html().replace(/\{@id\}/g,'a'+maxid));
        $('#a'+maxid+' .operations .btn').tooltip();
    });

    $('#column-table').on('click','.btn-delete', function(){
        var id=parseInt($(this).data('id'));
        var row = $(this).parents('tr');
        dialog.confirm($(this).data('confirm').replace('\\n',"<br />"),function(){
            if(id){
                $.ajax({
                    url:"{:url('init',['act'=>'delete','id'=>'__ID__'])}".replace('__ID__',id),
                    dataType:'json',
                    success:function(json){
                        if(json.code == 1){
                            dialog.success('删除成功');
                            row.remove();
                        }else{
                            dialog.error(json.msg);
                        }
                    }
                })
            }else{
                row.remove();
            }
        });
    })
})
</script>
{/block}