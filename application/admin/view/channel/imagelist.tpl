{extend name="public:base" /}

{block name="body"}

{include file="channel/_bread" title="图集列表" /}

<div id="page-wrapper" class="container-fluid">
    
    <div class="row list-header">
        <div class="col-6">
            <a href="{:url('channel/index',['channel_id'=>$channel_id])}" class="btn btn-outline-primary btn-sm"><i class="ion-md-arrow-back"></i> 返回列表</a>
            <a href="{:url('channel/imageadd',array('channel_id'=>$channel_id,'aid'=>$aid))}" class="btn btn-outline-primary btn-sm upload-image"><i class="ion-md-add"></i> 添加图片</a>
        </div>
        <div class="col-6">
            <form action="{:url('channel/imagelist',['channel_id'=>$channel_id])}" method="post">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" name="key" placeholder="输入标题或者地址关键词搜索">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="submit"><i class="ion-md-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        {empty name="lists"}<div class="col-12 p-5 text-center text-muted">还没有上传图片哦~</div>{/empty}
        {foreach $lists as $key=>$v}
            <div class="col-12 col-md-6 col-lg-3 mb-4">
            <div class="card">
                <figure class="card-img-top figure img-view" data-img="{$v.image}" >
                    <img src="{$v.image|default='/static/images/nopic.png'}?w=500" class="figure-img img-fluid rounded" alt="image">
                </figure>
                <div class="card-body">
                    <div class="card-text">{$v.title|default='无标题'}</div>
                    <div class="text-muted">{$v.description|default='暂无说明'}</div>
                </div>
                <div class="operations">
                    <div class="btn-group btn-group-sm float-right">
                    <a class="btn btn-outline-primary upload-image" title="编辑" href="{:url('channel/imageupdate',array('channel_id'=>$channel_id,'id'=>$v['id'],'aid'=>$aid))}"><i class="ion-md-create"></i> </a>
                    <a class="btn btn-outline-danger link-confirm" title="删除" data-confirm="您真的确定要删除吗？\n删除后将不能恢复!" href="{:url('channel/imagedelete',array('id'=>$v['id'],'aid'=>$aid))}"><i class="ion-md-trash"></i> </a>
                </div>
                </div>
              </div>
            </div>
        {/foreach}
    </div>
    {$page|raw}
</div>
{/block}