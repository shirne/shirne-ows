
<div class="form-row form-group">
    <label for="v-m_open" class="col-3 col-md-2 text-right align-middle">会员系统</label>
    <div class="col-9 col-md-8 col-lg-6">
        <div class="btn-group btn-group-toggle mopengroup" data-toggle="buttons">
            {foreach $setting['m_open']['data']??[] as $k => $value}
                {if $k==$setting['m_open']['value']}
                    <label class="btn btn-outline-secondary active">
                        <input type="radio" name="v-m_open" value="{$k}" autocomplete="off" checked> {$value}
                    </label>
                    {else /}
                    <label class="btn btn-outline-secondary">
                        <input type="radio" name="v-m_open" value="{$k}" autocomplete="off"> {$value}
                    </label>
                {/if}
            {/foreach}
        </div>
    </div>
</div>
<div class="form-row form-group">
    <label for="v-m_register_open" class="col-3 col-md-2 text-right align-middle">开启注册</label>
    <div class="col-9 col-md-8 col-lg-6">
        <div class="btn-group btn-group-toggle mregopengroup" data-toggle="buttons">
            {foreach $setting['m_register_open']['data']??[] as $k => $value}
                {if $k==$setting['m_register_open']['value']}
                    <label class="btn btn-outline-secondary active">
                        <input type="radio" name="v-m_register_open" value="{$k}" autocomplete="off" checked> {$value}
                    </label>
                    {else /}
                    <label class="btn btn-outline-secondary">
                        <input type="radio" name="v-m_register_open" value="{$k}" autocomplete="off"> {$value}
                    </label>
                {/if}
            {/foreach}
        </div>
    </div>
</div>
<div class="form-row form-group regsetrow">
    <label for="v-m_register" class="col-3 col-md-2 text-right align-middle">强制注册</label>
    <div class="col-9 col-md-8 col-lg-6">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
        {foreach $setting['m_register']['data']??[] as $k => $value}
            {if $k==$setting['m_register']['value']}
                <label class="btn btn-outline-secondary active">
                    <input type="radio" name="v-m_register" value="{$k}" autocomplete="off" checked> {$value}
                </label>
                {else /}
                <label class="btn btn-outline-secondary">
                    <input type="radio" name="v-m_register" value="{$k}" autocomplete="off"> {$value}
                </label>
            {/if}
        {/foreach}
        </div>
        <div class="text-muted">强制注册开启时，当用户从第三方授权登录(如：微信，QQ等)后，会进入临时账号状态，需要绑定或注册系统账号才能正常使用</div>
    </div>
</div>
<div class="form-row form-group regsetrow">
    <label for="v-m_invite" class="col-3 col-md-2 text-right align-middle">邀请注册</label>
    <div class="col-9 col-md-8 col-lg-6">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            {foreach $setting['m_invite']['data']??[] as $k => $value}
                {if $k==$setting['m_invite']['value']}
                    <label class="btn btn-outline-secondary active">
                        <input type="radio" name="v-m_invite" value="{$k}" autocomplete="off" checked> {$value}
                    </label>
                    {else /}
                    <label class="btn btn-outline-secondary">
                        <input type="radio" name="v-m_invite" value="{$k}" autocomplete="off"> {$value}
                    </label>
                {/if}
            {/foreach}
        </div>
        <div class="text-muted">邀请注册关闭后，所有通过分享关系进入的会员，将不再绑定。开启时有分享人则绑定。强制邀请注册时则只能从系统的邀请码注册</div>
    </div>
</div>
<div class="form-row form-group regsetrow">
        <label for="v-anonymous_comment" class="col-3 col-md-2 text-right align-middle">匿名评论</label>
        <div class="col-9 col-md-8 col-lg-6">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                {foreach $setting['anonymous_comment']['data']??[] as $k => $value}
                    {if $k==$setting['anonymous_comment']['value']}
                        <label class="btn btn-outline-secondary active">
                            <input type="radio" name="v-anonymous_comment" value="{$k}" autocomplete="off" checked> {$value}
                        </label>
                        {else /}
                        <label class="btn btn-outline-secondary">
                            <input type="radio" name="v-anonymous_comment" value="{$k}" autocomplete="off"> {$value}
                        </label>
                    {/if}
                {/foreach}
            </div>
            <div class="text-muted">文章评论是否需要会员登录</div>
        </div>
    </div>
<div class="form-row form-group">
    <label for="v-m_checkcode" class="col-3 col-md-2 text-right align-middle">验证码</label>
    <div class="col-9 col-md-8 col-lg-6">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
        {foreach $setting['m_checkcode']['data']??[] as $k => $value}
            {if $k==$setting['m_checkcode']['value']}
                <label class="btn btn-outline-secondary active">
                    <input type="radio" name="v-m_checkcode" value="{$k}" autocomplete="off" checked> {$value}
                </label>
                {else /}
                <label class="btn btn-outline-secondary">
                    <input type="radio" name="v-m_checkcode" value="{$k}" autocomplete="off"> {$value}
                </label>
            {/if}
        {/foreach}
        </div>
    </div>
</div>