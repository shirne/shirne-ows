<div class="form-row form-group">
    <label for="v-sitemap_html" class="col-3 col-md-2 text-right align-middle">{$setting['sitemap_html']['title']}</label>
    <div class="col-6 col-md-4 col-lg-3">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            {foreach $setting['sitemap_html']['data']??[] as $k => $value}
                {if $k==$setting['sitemap_html']['value']}
                    <label class="btn btn-outline-secondary active">
                        <input type="radio" name="v-sitemap_html" value="{$k}" autocomplete="off" checked> {$value}
                    </label>
                    {else /}
                    <label class="btn btn-outline-secondary">
                        <input type="radio" name="v-sitemap_html" value="{$k}" autocomplete="off"> {$value}
                    </label>
                {/if}
            {/foreach}
        </div>
    </div>
    <div class="text-muted">{$setting['sitemap_html']['description']}</div>
</div>
<div class="form-row form-group">
    <label for="v-sitemap_mode" class="col-3 col-md-2 text-right align-middle">{$setting['sitemap_mode']['title']}</label>
    <div class="col-6 col-md-4 col-lg-3">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            {foreach $setting['sitemap_mode']['data']??[] as $k => $value}
                {if $k==$setting['sitemap_mode']['value']}
                    <label class="btn btn-outline-secondary active">
                        <input type="radio" name="v-sitemap_mode" value="{$k}" autocomplete="off" checked> {$value}
                    </label>
                    {else /}
                    <label class="btn btn-outline-secondary">
                        <input type="radio" name="v-sitemap_mode" value="{$k}" autocomplete="off"> {$value}
                    </label>
                {/if}
            {/foreach}
        </div>
    </div>
    <div class="text-muted">{$setting['sitemap_mode']['description']}</div>
</div>
<div class="form-row form-group">
    <label for="v-lang_switch" class="col-3 col-md-2 text-right align-middle">{$setting['lang_switch']['title']}</label>
    <div class="col-6 col-md-4 col-lg-3">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            {foreach $setting['lang_switch']['data']??[] as $k => $value}
                {if $k==$setting['lang_switch']['value']}
                    <label class="btn btn-outline-secondary active">
                        <input type="radio" class="lang_switch" name="v-lang_switch" value="{$k}" autocomplete="off" checked> {$value}
                    </label>
                    {else /}
                    <label class="btn btn-outline-secondary">
                        <input type="radio" class="lang_switch" name="v-lang_switch" value="{$k}" autocomplete="off"> {$value}
                    </label>
                {/if}
            {/foreach}
        </div>
    </div>
    <div class="text-muted">{$setting['lang_switch']['description']}</div>
</div>

<div class="form-row form-group lang-list">
    <label for="v-lang_data" class="col-3 col-md-2 text-right align-middle">{$setting['lang_data']['title']}</label>
    <div class="col-12 col-md-8 col-lg-6">
        {foreach :empty($setting['lang_data']['value'])?[['name'=>'zh-cn','title'=>'简体中文']]:$setting['lang_data']['value'] as $key=>$value}
        <div class="input-group mb-2" data-key="{$key}">
            <span class="input-group-prepend"><span class="input-group-text">语言标识</span></span>
            <input type="text" class="form-control" name="v-lang_data[{$key}][name]" value="{$value['name']}" placeholder="">
            <span class="input-group-middle"><span class="input-group-text">语言标题</span></span>
            <input type="text" class="form-control" name="v-lang_data[{$key}][title]" value="{$value['title']}" placeholder="">
            <div class="input-group-append">
            <a class="btn btn-outline-secondary delkeepbtn" href="javascript:">移除</a>
            </div>
        </div>
        {/foreach}
        <a href="javascript:" class="btn btn-primary addlangbtn">添加语言</a>
    </div>
    <div class="text-muted">{$setting['lang_data']['description']}</div>
</div>
<script type="text/html" id="lang_tpl">
    <div class="input-group mb-2" data-key="{@key}">
        <span class="input-group-prepend"><span class="input-group-text">语言标识</span></span>
        <input type="text" class="form-control" name="v-lang_data[{@key}][name]" value="" placeholder="">
        <span class="input-group-middle"><span class="input-group-text">语言标题</span></span>
        <input type="text" class="form-control" name="v-lang_data[{@key}][title]" value="" placeholder="">
        <div class="input-group-append">
            <a class="btn btn-outline-secondary dellangbtn" href="javascript:">移除</a>
        </div>
    </div>
</script>
<script>
jQuery(function($){
    var maxkey=1;
    var lastgroup=$('.lang-list .input-group').eq(-1)
    if(lastgroup.length>0){
        maxkey = lastgroup.data('key')+1;
    }
    var tpl = $('#lang_tpl').html();
    $('.addlangbtn').click(function(e){
        $(tpl.compile({key: maxkey})).insertBefore(this)
        maxkey++
    });

    $('.lang-list').on('click','.dellangbtn',function(e){
        var row = $(this).parents('.input-group');
        dialog.confirm('确定删除该语言？',function () {
            row.remove();
        })
    })

    $('.lang_switch').change(function(e){
        if($(this).val()==1){
            $('.lang-list').show();
        }else{
            $('.lang-list').hide();
        }
    }).filter(':checked').trigger('change');
})
</script>