<?php

namespace app\admin\model;

use shirne\third\ThirdBase;

class VersionModel extends ThirdBase{

    protected $appname = '';
    protected $appversion = '';

    public function __construct($options)
    {
        parent::__construct($options);

        $this->appname = config('app.app_name');
        $this->appversion = config('app.app_release');
    }

    public function getVersion(){
        return $this->appname.' v'.$this->appversion;
    }

    public function checkUpdate(){
        $result = $this->http(config('app.app_host').'check', ['appname'=>$this->appname,'appversion'=>$this->appversion]);
        if(!empty($result)){
            $json = json_decode($result, true);
            return $json['data'];
        }
        return [];
    }
    
    public function doUpdate(){
        //todo
    }
}