<?php

namespace app\admin\validate;


use app\common\core\BaseUniqueValidate;

/**
 * 文章字段验证
 * Class ArticleFieldsValidate
 * @package app\admin\validate
 */
class ArticleFieldsValidate extends BaseUniqueValidate
{
    protected $rule=array(
        'name'=>['require','unique'=>'articleFields,%id%', 'regex' => '/^[a-z][a-z0-9_]+$/','check_hold'=>''],
        'title'=>'require'
    );
    protected $message=array(
        'name.require'=>'请填写字段名',
        'title.require'=>'请填写字段标题',
        'name.regex'=>'请填写小写字母+数字格式',
        'name.check_hold'=>'字段名与系统保留字段冲突'
    );

    public function check_hold($value, $data){
        if(in_array($value,['id','lang','main_id','channel_id','cate_id','user_id','name','title','vice_title','keywords','cover','description','prop_data','content','create_time','update_time','digg','v_digg','comment','views','v_views','sort','type','template','status',
        'cate_name','channel_name']) !== false){
            return false;
        }
        return true;
    }
}